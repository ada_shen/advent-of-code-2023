//go:build gears_2
// +build gears_2

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// The type of a component in the input schematic
type ComponentType uint8

const (
	Number ComponentType = iota // A numeric component (may or may not be a part number)
	Gear                        // A gear (asterisk). May or may not actually be connected to two numbers.
)

func (cType ComponentType) String() string {
	if cType == Number {
		return "Number"
	} else {
		return "Gear"
	}
}

// A component in the input schematic. May span multiple coordinates.
type Component struct {
	Typeof      ComponentType       // The type of this Component
	Value       int                 // The parsed value of this Component if it's a Number (is always 0 for Symbols)
	Connections map[*Component]bool // The set of Numbers to which this Component is connected if it's a Gear
}

// Makes a new component of the given type
func NewComponent(typeof ComponentType) *Component {
	component := Component{Typeof: typeof}

	if typeof == Gear {
		component.Connections = make(map[*Component]bool)
	}

	return &component
}

func main() {
	total := 0
	var prevLineComponents []*Component
	var currentLineComponents []*Component
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			break
		}

		prevLineComponents = currentLineComponents
		currentLineComponents = make([]*Component, utf8.RuneCountInString(line))

		var numberBuilder strings.Builder

		// Takes the current position on the line and finishes any buffered number present in numberBuilder
		finishNumberIfPresent := func(pos int) {
			numberLength := numberBuilder.Len()
			if numberLength == 0 {
				return
			}

			numberStart := pos - numberLength
			numberEnd := pos - 1

			component := NewComponent(Number)
			component.Value, _ = strconv.Atoi(numberBuilder.String())
			numberBuilder.Reset()

			// Insert the Number Component into the data structure at each position along its length
			for i := numberStart; i <= numberEnd; i++ {
				currentLineComponents[i] = component
			}

			var prevComponent *Component
			if numberStart > 0 {
				prevComponent = currentLineComponents[numberStart-1]
			}

			if prevComponent != nil && prevComponent.Typeof == Gear { // Check prev component on this line
				prevComponent.Connections[component] = true
			}

			if prevLineComponents != nil {
				// Check any components adjacent to this Number on the previous line
				for i := max(0, numberStart-1); i <= numberEnd+1 && i < len(prevLineComponents); i++ {
					aboveComponent := prevLineComponents[i]
					if aboveComponent != nil && aboveComponent.Typeof == Gear {
						aboveComponent.Connections[component] = true
					}
				}
			}
		}

		for pos, char := range line {
			if unicode.IsDigit(char) {
				// If we see a digit, just add it to the builder and continue
				numberBuilder.WriteRune(char)
			} else {
				// Finish any number that just ended
				finishNumberIfPresent(pos)

				if char == '*' {
					component := NewComponent(Gear)
					currentLineComponents[pos] = component

					// Basically the same logic as when finishing a number, but here we're looking for Numbers
					prevComponent := currentLineComponents[pos-1]
					if prevComponent != nil && prevComponent.Typeof == Number {
						component.Connections[prevComponent] = true
					}

					if prevLineComponents != nil {
						for i := max(0, pos-1); i <= pos+1 && i < len(prevLineComponents); i++ {
							aboveComponent := prevLineComponents[i]
							if aboveComponent != nil && aboveComponent.Typeof == Number {
								component.Connections[aboveComponent] = true
							}
						}
					}
				}

				// Even if we saw a second connection to a Gear, don't eagerly add anything to the total.
				// Gears are defined as having *exactly* two connections, so we could find a third later.
			}
		}

		// Make sure to finish any number at the end of the line
		finishNumberIfPresent(len(line))

		// Now we can guarantee that all the components on the *previous* line (if there is one)
		// are fully connected, so loop through it and add any properly connected Gears to the total
		if prevLineComponents != nil {
			for _, component := range prevLineComponents {
				if component != nil && component.Typeof == Gear && len(component.Connections) == 2 {
					ratio := 1
					for component, _ := range component.Connections {
						ratio *= component.Value
					}

					total += ratio
				}
			}
		}
	}

	// Check the last line at the very end
	for _, component := range currentLineComponents {
		if component != nil && component.Typeof == Gear && len(component.Connections) == 2 {
			ratio := 1
			for component, _ := range component.Connections {
				ratio *= component.Value
			}

			total += ratio
		}
	}

	fmt.Println(total)
}
