mutable struct Mapping
    const key::AbstractString
    const initial::Bool
    const terminal::Bool
    left::Union{Mapping, Nothing}
    right::Union{Mapping, Nothing}
end

Base.show(io::IO, m::Mapping) = print(io, m.key, " => (", m.left.key, ", ", m.right.key, ")")

mapping_regex = r"^(\w{3}) = \((\w{3}), (\w{3})\)$"

function parse_mappings(lines::AbstractVector{String})::Vector{Mapping}
    mappings = Dict{String, Mapping}()

    function get_or_create_mapping(key::AbstractString)
        get!(mappings, key) do
            Mapping(key, key[end] == 'A', key[end] == 'Z', nothing, nothing)
        end
    end

    for line in lines
        line_match = match(mapping_regex, line)

        (mapping, left, right) = get_or_create_mapping.(line_match.captures)
        mapping.left = left
        mapping.right = right
    end

    collect(values(mappings))
end

function find_traversal_length(initial_position::Mapping, directions::String)::Integer
    steps = 0
    pos = initial_position
    while true
        for dir in directions
            steps += 1
            pos = dir == 'L' ? pos.left : pos.right
            if pos.terminal
                return steps
            end
        end
    end
end

open("wasteland-input.txt") do input
    lines = readlines(input)

    # The first line is the LR directions, followed by a blank line, and then the mappings
    directions = lines[1]
    initial_positions = parse_mappings(@view lines[3:end]) |> filter(m -> m.initial)

    # Embarrassed to say I had to look up an explanation for this one...
    # The trick to avoid actually having to perform the full gigantic traversal
    # is to first find the steps it takes to get from each starting point to a terminal point,
    # then the number of steps it will take for them all to line up
    # is just the least common multiple of those values.
    result = lcm(find_traversal_length.(initial_positions, directions)...)
    println(result)
end
