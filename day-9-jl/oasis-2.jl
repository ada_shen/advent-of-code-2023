function extrapolate_back(values::AbstractVector{<:Integer})::Integer
    diffs = diff(values)
    diff_back = all(iszero, diffs) ? 0 : extrapolate_back(diffs)
    values[1] - diff_back
end

open("oasis-input.txt") do input
    lines = readlines(input)

    total = 0
    for line in lines
        values = parse.(Int, split(line))
        total += extrapolate_back(values)
    end

    println(total)
end
