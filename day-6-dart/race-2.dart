import "dart:io";
import "dart:math";

final timePattern = RegExp(r"^Time:\s+(.*)$");
final distancePattern = RegExp(r"^Distance:\s+(.*)$");
final whitespacePattern = RegExp(r"\s+");

void main() {
    final [timeLine, distanceLine] = File("race-input.txt").readAsLinesSync();

    final timeStr = timePattern.firstMatch(timeLine)![1]!.replaceAll(whitespacePattern, "");
    final time = int.parse(timeStr);

    final distanceStr = distancePattern.firstMatch(distanceLine)![1]!.replaceAll(whitespacePattern, "");
    final distance = int.parse(distanceStr);

    //(time - X) * X > distance, solve for X
    //Is it cheating to use Wolfram|Aplha, lol?

    //IDK if this case will ever occur in the input, but it's not possible if this is true
    if (time <= 2 * sqrt(distance)) {
        print(0);
        return;
    }

    //0.5 * (time - sqrt(time ** 2 - 4 * distance)) < X < 0.5 * (sqrt(time ** 2 - 4 * distance) + time)
    final root = sqrt(time * time - 4 * distance);

    //The formula gives exclusive upper/lower bounds which may not be integers.
    //To get inclusive integer bounds, floor/ceiling away from X and then add/subtract 1 towards it.
    final minSolution = (0.5 * (time - root)).floor() + 1;
    final maxSolution = (0.5 * (root + time)).ceil() - 1;

    final numSolutions = maxSolution - minSolution + 1;

    print(numSolutions);
}
