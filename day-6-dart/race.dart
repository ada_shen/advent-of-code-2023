import "dart:io";
import "dart:math";

final timesPattern = RegExp(r"^Time:\s+(.*)$");
final distancesPattern = RegExp(r"^Distance:\s+(.*)$");
final whitespacePattern = RegExp(r"\s+");

void main() {
    final [timesStr, distancesStr] = File("race-input.txt").readAsLinesSync();
    final times = timesPattern.firstMatch(timesStr)![1]!.split(whitespacePattern).map(int.parse).toList();
    final distances = distancesPattern.firstMatch(distancesStr)![1]!.split(whitespacePattern).map(int.parse).toList();

    var result = 1;
    for (var i = 0; i < times.length; i++) {
        final time = times[i];
        final distance = distances[i];

        //(time - X) * X > distance, solve for X
        //Is it cheating to use Wolfram|Aplha, lol?

        //IDK if this case will ever occur in the input, but it's not possible if this is true
        if (time <= 2 * sqrt(distance)) {
            result = 0;
            break;
        }

        //0.5 * (time - sqrt(time ** 2 - 4 * distance)) < X < 0.5 * (sqrt(time ** 2 - 4 * distance) + time)
        final root = sqrt(time * time - 4 * distance);

        //The formula gives exclusive upper/lower bounds which may not be integers.
        //To get inclusive integer bounds, floor/ceiling away from X and then add/subtract 1 towards it.
        final minSolution = (0.5 * (time - root)).floor() + 1;
        final maxSolution = (0.5 * (root + time)).ceil() - 1;

        final numSolutions = maxSolution - minSolution + 1;
        result *= numSolutions;
    }

    print(result);
}
