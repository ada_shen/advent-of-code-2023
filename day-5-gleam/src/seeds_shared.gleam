import gleam/dict.{type Dict}
import gleam/int
import gleam/list
import gleam/option.{Some}
import gleam/regex.{Match}
import gleam/string

pub type Mapping {
    Mapping(from: String, to: String, entries: List(MapEntry))
}

pub type MapEntry {
    MapEntry(start: Int, end: Int, offset: Int)
}

pub fn parse_mappings(lines: List(String)) -> Dict(String, Mapping) {
    parse_mappings_internal(lines, dict.new())
}

fn parse_mappings_internal(lines: List(String), accum: Dict(String, Mapping)) -> Dict(String, Mapping) {
    let assert Ok(map_header_pattern) = regex.from_string("(\\w+)-to-(\\w+)")

    let assert [header, ..lines] = lines
    let assert [Match(_, [Some(from), Some(to)])] = regex.scan(header, with: map_header_pattern)

    let #(entry_lines, lines) = lines |> list.split_while(fn(line) { line != "" })
    let entries = entry_lines |> list.map(fn(line) {
        let assert [Ok(output_start), Ok(input_start), Ok(range_length)] =
            line
            |> string.split(on: " ")
            |> list.map(int.parse)
        let input_end = input_start + range_length - 1
        let offset = output_start - input_start
        MapEntry(input_start, input_end, offset)
    })

    let mapping = Mapping(from, to, entries)
    let accum = accum |> dict.insert(from, mapping)

    let lines = lines |> list.drop_while(fn(line) { line == "" })
    case lines {
        [] -> accum
        _ -> parse_mappings_internal(lines, accum)
    }
}
