# frozen_string_literal: true

# Struct containing an array of cards (characters) and a bid (int)
Hand = Struct.new(:cards, :bid) do
    # Hash mapping characters to their ordering
    CARD_ORDER = 'J123456789TQKA'.each_char.each_with_index.to_h

    def hand_type
        @hand_type ||= begin
            # We can tell a lot by just looking at the number of each card in the hand
            card_counts = cards.tally

            if card_counts.has_key?('J')
                j_count = card_counts.delete('J')
                if j_count == 5
                    card_counts['A'] = 5
                else
                    max_other_card = card_counts.max_by { _2 }[0]
                    card_counts[max_other_card] += j_count
                end
            end

            case card_counts.size
            when 1
                # If there's only one kind of card in hand, it's a five of a kind
                HandType::FIVE_OF_KIND
            when 2
                # Two card types in hand can be either four of a kind or a full house, depending on the counts
                if card_counts.values.max == 4
                    # Four of a kind is 4 + 1
                    HandType::FOUR_OF_KIND
                else
                    # And full house is 3 + 2
                    HandType::FULL_HOUSE
                end
            when 3
                # Three card types can be three of a kind plus two cards, or two pairs plus a single card
                if card_counts.values.max == 3
                    HandType::THREE_OF_KIND
                else
                    HandType::TWO_PAIR
                end
            when 4
                # If there are four, then we have one pair (and three single cards)
                HandType::ONE_PAIR
            when 5
                # All five cards are different
                HandType::HIGH_CARD
            end
        end
    end

    def <=>(other)
        type_order = hand_type <=> other.hand_type
        return type_order unless type_order == 0

        cards.zip(other.cards) do |(card, other_card)|
            card_order = CARD_ORDER[card] <=> CARD_ORDER[other_card]
            return card_order unless card_order == 0
        end

        0
    end

    def to_s = "#<struct Hand cards=#{cards.join} bid=#{bid} hand_type=#{HandType.name(hand_type)}>"

    # Very annoying that Structs don't let you override this with inspect like normal
    def pretty_print(pp)
        pp.group(1, '<struct Hand', '>') do
            pp.breakable
            pp.text "cards="
            pp.text cards.join

            pp.breakable
            pp.text 'bid='
            pp.pp bid

            pp.breakable
            pp.text 'hand_type='
            pp.text HandType.name(hand_type)
        end
    end
end

module HandType
    HIGH_CARD = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE_OF_KIND = 3
    FULL_HOUSE = 4
    FOUR_OF_KIND = 5
    FIVE_OF_KIND = 6

    def self.name(hand_type)
        case hand_type
        when HIGH_CARD then 'HIGH_CARD'
        when ONE_PAIR then 'ONE_PAIR'
        when TWO_PAIR then 'TWO_PAIR'
        when THREE_OF_KIND then 'THREE_OF_KIND'
        when FULL_HOUSE then 'FULL_HOUSE'
        when FOUR_OF_KIND then 'FOUR_OF_KIND'
        when FIVE_OF_KIND then 'FIVE_OF_KIND'
        end
    end
end

input = File.readlines('camel-input.txt')
HANDS = input.map do |line|
    (cards_str, bid_str) = line.rstrip.split
    Hand.new(cards_str.each_char.to_a, bid_str.to_i)
end

result = HANDS.sort.each_with_index.map { |hand, i| hand.bid * (i + 1) }.sum
puts result
