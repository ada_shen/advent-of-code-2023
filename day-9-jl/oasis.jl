function extrapolate(values::AbstractVector{<:Integer})::Integer
    diffs = diff(values)
    next_diff = all(iszero, diffs) ? 0 : extrapolate(diffs)
    values[end] + next_diff
end

open("oasis-input.txt") do input
    lines = readlines(input)

    total = 0
    for line in lines
        values = parse.(Int, split(line))
        total += extrapolate(values)
    end

    println(total)
end
