import gleam/dict.{type Dict}
import gleam/int
import gleam/io
import gleam/list
import gleam/option.{Some}
import gleam/pair
import gleam/regex.{Match}
import gleam/result
import gleam/string

import seeds_shared.{type MapEntry, type Mapping, Mapping} as shared

pub fn day_2(input: String) -> Nil {
    let assert Ok(seeds_pattern) = regex.from_string("^seeds: (.*)$")

    let lines = string.split(input, on: "\n")

    let assert [seeds_line, _, ..lines] = lines
    let assert [Match(_, [Some(seeds_str)])] = regex.scan(seeds_line, with: seeds_pattern)
    let seed_ranges =
        seeds_str
        |> string.split(on: " ")
        |> list.map(fn(str) {
            let assert Ok(seed) = int.parse(str)
            seed
        })
        |> list.sized_chunk(2)
        |> list.map(fn(pair) {
            let assert [start, length] = pair
            #(start, start + length - 1)
        })

    let mappings = shared.parse_mappings(lines)

    let assert Ok(result) =
        expand_ranges(seed_ranges, mappings, "seed")
        |> list.map(pair.first)
        |> list.reduce(int.min)

    io.debug(result)

    Nil
}

fn expand_ranges(ranges: List(#(Int, Int)), mappings: Dict(String, Mapping), category: String) -> List(#(Int, Int)) {
    let assert Ok(Mapping(_, output_category, map_entries)) = mappings |> dict.get(category)

    let expansion =
        ranges
        |> list.flat_map(expand_range(_, map_entries))
        //It's possible for the expansion to contain overlapping ranges
        //if an unmapped input range corresponds with the output of a mapped range.
        //It's not necessary to collapse these to get the correct answer,
        //but doing so prevents some duplicated work.
        |> collapse_overlapping_ranges

    case output_category {
        "location" -> expansion
        _ -> expand_ranges(expansion, mappings, output_category)
    }
}

fn collapse_overlapping_ranges(ranges: List(#(Int, Int))) -> List(#(Int, Int)) {
    case ranges |> list.sort(by: fn(a, b) { int.compare(a.0, b.0) }) {
        [] | [_] -> ranges
        [head, ..tail] -> collapse_overlapping_ranges_impl(tail, head, [])
    }
}

fn collapse_overlapping_ranges_impl(
    ranges: List(#(Int, Int)),
    range_accum: #(Int, Int),
    list_accum: List(#(Int, Int))
) -> List(#(Int, Int)) {
    case ranges {
        [] -> [range_accum, ..list_accum]
        [next_range, ..rest] ->
            case next_range.0 <= range_accum.1 {
                True -> collapse_overlapping_ranges_impl(rest, #(range_accum.0, next_range.1), list_accum)
                False -> collapse_overlapping_ranges_impl(rest, next_range, [range_accum, ..list_accum])
            }
    }
}

fn expand_range(range: #(Int, Int), map_entries: List(MapEntry)) -> List(#(Int, Int)) {
    let mapped_input_ranges =
        map_entries
        |> list.filter_map(fn(entry) {
            //First, see if the entry contains the start of the input range, and if so find the overlap
            case range.0 >= entry.start && range.0 <= entry.end {
                True -> Ok(#(range.0, int.min(range.1, entry.end), entry.offset)) //Store the offset for later
                False -> Error(Nil)
            }
            |> result.try_recover(fn(_) {
                //If no hits there, check the end of the range
                case range.1 >= entry.start && range.1 <= entry.end {
                    True -> Ok(#(int.max(range.0, entry.start), range.1, entry.offset))
                    False -> Error(Nil)
                }
            })
            |> result.try_recover(fn(_) {
                //Finally, check for the case where the input range *fully contains* the map entry
                case entry.start >= range.0 && entry.start <= range.1 {
                    True -> Ok(#(entry.start, entry.end, entry.offset))
                    False -> Error(Nil)
                }
            })
        })
        |> list.sort(by: fn(a, b) { //Need to sort in order to find unmapped gaps
            //We know the ranges are disjoint so we can just sort by their start points
            int.compare(a.0, b.0)
        })

    //Wrap the list of mapped input ranges in the original input start and end points,
    //then scan down the list, recording any gaps as unmapped ranges.
    let unmapped_input_ranges =
        [#(0, range.0 - 1, 0), ..{ mapped_input_ranges |> list.append([#(range.1 + 1, 0, 0)]) }]
        |> list.window_by_2
        |> list.filter_map(fn(pair) {
            let #(prev, next) = pair
            case next.0 - prev.1 {
                1 -> Error(Nil)
                _ -> Ok(#(prev.1 + 1, next.0 - 1))
            }
        })

    mapped_input_ranges
    |> list.map(fn(range) { #(range.0 + range.2, range.1 + range.2) }) //Apply the stored offsets
    |> list.append(unmapped_input_ranges)
}
