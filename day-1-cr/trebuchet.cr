start_pattern = /^\D*(\d)/
end_pattern = /(\d)\D*$/

total = 0
STDIN.each_line do |line|
    break if line.empty?

    start_digit = start_pattern.match!(line)[1]
    end_digit = end_pattern.match!(line)[1]
    total += (start_digit + end_digit).to_i
end

puts total
