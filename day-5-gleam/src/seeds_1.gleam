import gleam/dict.{type Dict}
import gleam/int
import gleam/io
import gleam/list
import gleam/option.{Some}
import gleam/regex.{Match}
import gleam/string

import seeds_shared.{type Mapping, Mapping} as shared

pub fn day_1(input: String) -> Nil {
    let assert Ok(seeds_pattern) = regex.from_string("^seeds: (.*)$")

    let lines = string.split(input, on: "\n")

    let assert [seeds_line, _, ..lines] = lines
    let assert [Match(_, [Some(seeds_str)])] = regex.scan(seeds_line, with: seeds_pattern)
    let seeds =
        seeds_str
        |> string.split(on: " ")
        |> list.map(fn(str) {
            let assert Ok(seed) = int.parse(str)
            seed
        })

    let mappings = shared.parse_mappings(lines)

    let assert Ok(result) =
        seeds
        |> list.map(seed_to_location(mappings, _))
        |> list.reduce(int.min)

    io.debug(result)

    Nil
}

fn seed_to_location(mappings: Dict(String, Mapping), seed: Int) -> Int {
    traverse(mappings, seed, "seed")
}

fn traverse(mappings: Dict(String, Mapping), input: Int, category: String) -> Int {
    let assert Ok(Mapping(_, output_category, map_entries)) = mappings |> dict.get(category)

    let output = case map_entries |> list.find(fn(entry) { input >= entry.start && input <= entry.end }) {
        Ok(entry) -> input + entry.offset
        _ -> input
    }

    case output_category {
        "location" -> output
        _ -> traverse(mappings, output, output_category)
    }
}
