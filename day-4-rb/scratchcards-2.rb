# frozen_string_literal: true

LINE_PATTERN = /^Card\s+\d+:\s+(.*?)\s+\|\s+(.*)$/

# Struct representing the winnings from one card in the input (the original plus the duplicates).
# `copies` is the number of additional copies won.
# `duration` is the number of cards this Winning is good for.
# (So, if you have 3 copies of a card with 4 winning numbers, it would generate `Winning.new(3, 4)`,
# then each subsequent card would add 3 copies and subtract 1 from `duration`, deleting the Winning if it hits 0.)
Winning = Struct.new(:copies, :duration)

total = 0
unused_winnings = []
STDIN.each_line do |line|
    break if line.empty?

    line_match = LINE_PATTERN.match(line)
    winning_nums = line_match[1].split.to_set
    have_nums = line_match[2].split.to_set

    win_count = (winning_nums & have_nums).size

    copies = 1 + unused_winnings.sum(&:copies)
    total += copies

    unused_winnings.reject! { (_1.duration -= 1).zero? }
    unused_winnings << Winning.new(copies, win_count) if win_count > 0
end

puts total
