import gleam/io

import argv
import simplifile

import seeds_1.{day_1}
import seeds_2.{day_2}

pub fn main() {
    case argv.load().arguments {
        ["1"] -> load_input() |> day_1
        ["2"] -> load_input() |> day_2
        _ -> {
            io.debug("Argument must be 1 or 2 to run the corresponding part")
            Nil
        }
    }
}

fn load_input() -> String {
    let assert Ok(input) = simplifile.read(from: "seeds-input.txt")
    input
}
