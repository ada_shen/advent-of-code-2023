# frozen_string_literal: true

LINE_PATTERN = /^Card\s+\d+:\s+(.*?)\s+\|\s+(.*)$/

total = 0
STDIN.each_line do |line|
    break if line.empty?

    line_match = LINE_PATTERN.match(line)
    winning_nums = line_match[1].split.to_set
    have_nums = line_match[2].split.to_set

    win_count = (winning_nums & have_nums).size
    total += 2 ** (win_count - 1) if win_count > 0
end

puts total
