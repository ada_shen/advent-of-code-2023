import kotlin.text.Regex

val linePattern = Regex("""^Game (\d+): (.*)$""")

val limits = mapOf(
    "red" to 12,
    "green" to 13,
    "blue" to 14
)

fun main() {
    var total = 0
    readLoop@while (true) {
        val line = readlnOrNull()
        if (line == null) break

        val lineMatch = linePattern.matchEntire(line)!!
        val gameId = lineMatch.groupValues[1].toInt()
        val subgames = lineMatch.groupValues[2].split("; ")
        for (subgame in subgames) {
            val pulls = subgame.split(", ")
            for (pull in pulls) {
                val (countStr, color) = pull.split(" ")
                val count = countStr.toInt()
                if (count > limits[color]!!) continue@readLoop
            }
        }

        total += gameId
    }

    println(total)
}
