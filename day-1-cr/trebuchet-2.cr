SIMPLE_DIGIT_PATTERN = /^\d$/
DIGIT_PATTERN = /\d|one|two|three|four|five|six|seven|eight|nine|zero/

DIGIT_LOOKUP = {
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9",
    "zero" => "0"
}

def convert_digit(raw_digit)
    return raw_digit if SIMPLE_DIGIT_PATTERN.matches?(raw_digit)
    DIGIT_LOOKUP[raw_digit]
end

total = 0
STDIN.each_line do |line|
    break if line.empty?

    digits = line.scan(DIGIT_PATTERN).map(&.[0])
    start_digit = convert_digit(digits.first)
    end_digit = convert_digit(digits.last)
    total += (start_digit + end_digit).to_i
end

puts total
