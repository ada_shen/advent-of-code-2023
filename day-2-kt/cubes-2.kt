import kotlin.text.Regex

val linePattern = Regex("""^Game (\d+): (.*)$""")

fun main() {
    var total = 0
    readLoop@while (true) {
        val line = readlnOrNull()
        if (line == null) break

        val lineMatch = linePattern.matchEntire(line)!!
        val subgames = lineMatch.groupValues[2].split("; ")

        val maxCounts = mutableMapOf(
            "red" to 0,
            "green" to 0,
            "blue" to 0
        )

        for (subgame in subgames) {
            val pulls = subgame.split(", ")
            for (pull in pulls) {
                val (countStr, color) = pull.split(" ")
                val count = countStr.toInt()
                maxCounts[color] = maxOf(maxCounts[color]!!, count)
            }
        }

        val power = maxCounts.values.reduce { a, b -> a * b }
        total += power
    }

    println(total)
}
