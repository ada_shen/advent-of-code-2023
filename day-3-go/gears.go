//go:build gears
// +build gears

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// The type of a component in the input schematic
type ComponentType uint8

const (
	Number ComponentType = iota // A numeric component (may or may not be a part number)
	Symbol                      // A single symbol (adjacent symbols are not combined)
)

func (cType ComponentType) String() string {
	if cType == Number {
		return "Number"
	} else {
		return "Symbol"
	}
}

// A component in the input schematic. May span multiple coordinates.
type Component struct {
	Typeof  ComponentType // The type of this Component
	Value   int           // The parsed value of this Component if it's a Number (is always 0 for Symbols)
	Counted bool          // True if this Component is a Number that has already been added to the total
}

// Makes a new component of the given type
func NewComponent(typeof ComponentType) *Component {
	return &Component{typeof, 0, false}
}

func main() {
	total := 0
	var prevLineComponents []*Component
	var currentLineComponents []*Component
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			break
		}

		prevLineComponents = currentLineComponents
		currentLineComponents = make([]*Component, utf8.RuneCountInString(line))

		var numberBuilder strings.Builder

		// Takes the current position on the line and finishes any buffered number present in numberBuilder
		finishNumberIfPresent := func(pos int) {
			numberLength := numberBuilder.Len()
			if numberLength == 0 {
				return
			}

			numberStart := pos - numberLength
			numberEnd := pos - 1

			component := NewComponent(Number)
			component.Value, _ = strconv.Atoi(numberBuilder.String())
			numberBuilder.Reset()

			// Insert the Number Component into the data structure at each position along its length
			for i := numberStart; i <= numberEnd; i++ {
				currentLineComponents[i] = component
			}

			var prevComponent *Component
			if numberStart > 0 {
				prevComponent = currentLineComponents[numberStart-1]
			}

			if prevComponent != nil && prevComponent.Typeof == Symbol { // Check prev component on this line
				total += component.Value
				component.Counted = true
			} else if prevLineComponents != nil {
				// Check any components adjacent to this Number on the previous line
				for i := max(0, numberStart-1); i <= numberEnd+1 && i < len(prevLineComponents); i++ {
					aboveComponent := prevLineComponents[i]
					if aboveComponent != nil && aboveComponent.Typeof == Symbol {
						total += component.Value
						component.Counted = true
						break
					}
				}
			}
		}

		for pos, char := range line {
			if unicode.IsDigit(char) {
				// If we see a digit, just add it to the builder and continue
				numberBuilder.WriteRune(char)
			} else {
				// Finish any number that just ended
				finishNumberIfPresent(pos)

				if char != '.' {
					currentLineComponents[pos] = NewComponent(Symbol)

					// Basically the same logic as when finishing a number,
					// but here we're looking for Numbers that haven't already been counted
					prevComponent := currentLineComponents[pos-1]
					if prevComponent != nil && prevComponent.Typeof == Number && !prevComponent.Counted {
						total += prevComponent.Value
						prevComponent.Counted = true
					}

					// A Symbol can attach to multiple Numbers, so we still need to keep looking,
					// even if we already found a Number to the left on the current line
					if prevLineComponents != nil {
						for i := max(0, pos-1); i <= pos+1 && i < len(prevLineComponents); i++ {
							aboveComponent := prevLineComponents[i]
							if aboveComponent != nil && aboveComponent.Typeof == Number && !aboveComponent.Counted {
								total += aboveComponent.Value
								aboveComponent.Counted = true
							}
						}
					}
				}
			}
		}

		// Make sure to finish any number at the end of the line
		finishNumberIfPresent(len(line))
	}

	fmt.Println(total)
}
