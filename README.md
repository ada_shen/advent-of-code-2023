# Advent of Code 2023

Working my way through [AoC 2023](https://adventofcode.com/2023) to flex my brain! Better late than never, as they say!

I'm selecting my programming language for each day by running [this list](https://gitlab.com/-/snippets/3696837)
through a random number generator. I'm allowing for repeats and for rerolling impractical/unfeasible languages,
so every single day might not be unique. Variety is the spice of life! ^_^
