mapping_regex = r"^(\w{3}) = \((\w{3}), (\w{3})\)$"

open("wasteland-input.txt") do input
    lines = readlines(input)

    # The first line is the LR directions, followed by a blank line
    directions = lines[1]

    mappings = Dict{String, Tuple{String, String}}()
    for line in @view lines[3:end]
        line_match = match(mapping_regex, line)
        key, left, right = line_match.captures
        mappings[key] = (left, right)
    end

    position = "AAA"
    steps = 0
    while position != "ZZZ"
        for dir in directions
            mapping = mappings[position]
            position = mapping[dir == 'L' ? 1 : 2]
            steps += 1

            if position == "ZZZ"
                break
            end
        end
    end

    println(steps)
end
